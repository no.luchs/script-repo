#!/bin/bash
##Donwload WP
cd /tmp && wget https://wordpress.org/latest.tar.gz
tar -xzvf latest.tar.gz
sudo cp -R wordpress /var/www/html/
sudo mkdir /var/www/html/wordpress/wp-content/uploads

## Create DB
# create random password

dbname="dbwordpress"
dbuser="dbuser"
dbpass="$(openssl rand -base64 12)"



mysql -e "CREATE DATABASE $dbname /*\!40100 DEFAULT CHARACTER SET utf8 */;"
mysql -e "CREATE USER $dbuser@localhost IDENTIFIED BY '${dbpass}';"
mysql -e "RANT ALL PRIVILEGES ON dbwordpress.* TO wpuser@localhost ;"
mysql -e "FLUSH PRIVILEGES;"



#DB Connect Wordpress
rm -R wordpress
#create wp config
cd /var/www/html/
cp wp-config-sample.php wp-config.php
#set database details with perl find and replace
sed -i -e "s/database_name_here/$dbname/g" wp-config.php
sed -i -e "s/username_here/$dbuser/g" wp-config.php
sed -i -e "s/password_here/\\Q${dbpass}/g" wp-config.php

## Config Wordpress
wpadmin="admin"
wpadminpw="admin"
wpmail="noah@luchsphoto.ch"
wpurl="localhost"
wptitle="no website test"

curl -d "weblog_title=$wptitle&user_name=$wpadmin&admin_password=$wpadminpw&admin_password2=$wpadminpw&admin_email=$wpmail" http://$wpurl/wp-admin/install.php?step=2

